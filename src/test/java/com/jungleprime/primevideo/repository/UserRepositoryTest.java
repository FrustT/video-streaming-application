package com.jungleprime.primevideo.repository;

import com.jungleprime.primevideo.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;



@DataJpaTest
class UserRepositoryTest {



    @Autowired
    private UserRepository testedUserRepository;

    @Test
    void itShouldFindUserByEmail() {
        // given
        User testUser = User.builder().name("Ms.Test").email("testEmail@mail.com").password("testPassword").verified(true).build();
        testedUserRepository.save(testUser);
        // when
        Optional<User> dbResponse = Optional.ofNullable(testedUserRepository.findByEmail("testEmail@mail.com"));
        // then
        assert dbResponse.isPresent();
    }
}