package com.jungleprime.primevideo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jungleprime.primevideo.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
