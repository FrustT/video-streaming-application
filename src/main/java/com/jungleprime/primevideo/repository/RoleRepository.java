package com.jungleprime.primevideo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jungleprime.primevideo.entity.UserRole;

public interface RoleRepository extends JpaRepository<UserRole, Long> {

}
