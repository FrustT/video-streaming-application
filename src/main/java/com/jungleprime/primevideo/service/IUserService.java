package com.jungleprime.primevideo.service;

import java.util.Collection;

import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.model.responses.UserResponse;

public interface IUserService {

    public Collection<UserResponse> findAll();

    public User findById(Long id);

    public User save(User user);// TODO change userCreateRequest

    public void deleteById(Long id);

    public User findByEmail(String email);

    public Boolean existsByEmail(String email);

    public User update(Long id,User user);

}
