package com.jungleprime.primevideo.service;

import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.exception.BusinessException;
import com.jungleprime.primevideo.model.responses.UserResponse;
import com.jungleprime.primevideo.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@AllArgsConstructor
@Service
public class UserService implements IUserService {

    private final PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Override
    public Collection<UserResponse> findAll() {
        return userRepository.findAll().stream().map(UserResponse::new).toList();
    }

    @Override
    public User findById(Long id) {
        Optional<User> dbResponse = userRepository.findById(id);
        if (dbResponse.isEmpty()) {
            throw new BusinessException(HttpStatus.NOT_FOUND, "User not found");
        }
        return dbResponse.get();

    }


    public User findByEmail(String email) {
        User dbResponse = userRepository.findByEmail(email);
        if (dbResponse == null) {
            throw new BusinessException(HttpStatus.NOT_FOUND, "User not found");
        }
        return dbResponse;

    }

    @Override
    public Boolean existsByEmail(String email) {
        Optional<User> dbResponse = Optional.ofNullable(userRepository.findByEmail(email));
        return dbResponse.isPresent();
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(Long id, User user) {
        User dbResponse = findById(id);
        dbResponse.setName(user.getName());
        dbResponse.setEmail(user.getEmail());
        dbResponse.setPassword(passwordEncoder.encode(user.getPassword()));
        dbResponse.setRoles(user.getRoles());
        dbResponse.setVerified(user.isVerified());
        dbResponse.setVerificationCode(user.getVerificationCode());
        dbResponse.setVerificationCodeExpiry(user.getVerificationCodeExpiry());
        dbResponse.setRecoveryCode(user.getRecoveryCode());
        dbResponse.setRecoveryCodeExpiry(user.getRecoveryCodeExpiry());
        return userRepository.save(dbResponse);
    }

    @Override
    public void deleteById(Long id) {
        User user = findById(id);
        userRepository.delete(user);
    }

}
