package com.jungleprime.primevideo.service;

import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.entity.UserPrincipal;
import com.jungleprime.primevideo.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Setter
@Service
public class PrincipalService implements UserDetailsService {

    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> dbResponseForUser = Optional.ofNullable(userRepository.findByEmail(username));
        if (dbResponseForUser.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        return fromUser(dbResponseForUser.get());
    }
    public UserPrincipal loadUserPrincipalByUsername(String username) throws UsernameNotFoundException {
        Optional<User> dbResponseForUser = Optional.ofNullable(userRepository.findByEmail(username));
        if (dbResponseForUser.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        return fromUser(dbResponseForUser.get());
    }
    public static UserPrincipal fromUser(User user) {
        return new UserPrincipal(user);
    }

    public UserPrincipal getCurrentUser() {
        return (UserPrincipal) org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
