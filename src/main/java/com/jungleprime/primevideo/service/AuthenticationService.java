package com.jungleprime.primevideo.service;


import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.entity.UserPrincipal;
import com.jungleprime.primevideo.exception.BusinessException;
import com.jungleprime.primevideo.model.requests.AuthenticationRequest;
import com.jungleprime.primevideo.model.requests.RegisterRequest;
import com.jungleprime.primevideo.model.responses.AuthenticationResponse;
import com.jungleprime.primevideo.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final IUserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest request) {
        var user = User.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .verified(request.getVerified())
                .roles(request.getRoles())
                .build();


        // check if user exists before register
        if(userService.existsByEmail(request.getEmail())) {
            throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "User already exists");
        }
        UserPrincipal response = UserPrincipal.fromUser(userService.save(user));
        var jwtToken = jwtService.generateToken(response.getUsername());
        return new AuthenticationResponse(jwtToken);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

        UserPrincipal userPrincipal = UserPrincipal.fromUser(userService.findByEmail(request.getUsername()));
        if (passwordEncoder.matches(request.getPassword(), userPrincipal.getPassword())) {
            String token = jwtService.generateToken(request.getUsername());
            return new AuthenticationResponse(token);
        }
        throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid username or password");
    }

    public void verify( String username , String verificationCode) {
            User user = userService.findByEmail(username);
            if (user.isVerified())
                throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "User already verified");
            if (user.getVerificationCode().equals(verificationCode) && user.getVerificationCodeExpiry().after(new Date(System.currentTimeMillis()))) {
                user.setVerified(true);
                user.setVerificationCodeExpiry(null);
                user.setVerificationCode(null);
                userService.save(user);
            } else {
                throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid verification code");
            }
    }


    public String verifyRequest(String email) {//adds a verificationCode and expiry to specified user

        User user = userService.findByEmail(email);
        if(user.isVerified())throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "User already verified");

        user.setVerificationCode(generateEnumSafeCode(email));
        user.setVerificationCodeExpiry(new Date(System.currentTimeMillis() + 600000));
        userService.save(user);
        return getUrl("/api/v1/auth/verify/" + email + "?code=" + user.getVerificationCode());
    }
    public void recoverPassword(String username, String recoveryCode, String password) {
        User user = userService.findByEmail(username);


        if (user.getRecoveryCode().equals(recoveryCode) && user.getRecoveryCodeExpiry().after(new Date(System.currentTimeMillis()))) {
            user.setPassword(passwordEncoder.encode(password));
            user.setRecoveryCodeExpiry(null);
            user.setRecoveryCode(null);
            userService.save(user);
        } else {
            throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid recovery code");
        }
    }
    public String recoverPasswordRequest(String email) {
        User user = userService.findByEmail(email);
        if(!user.isVerified())throw new BusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "User not verified");

        user.setRecoveryCode(generateEnumSafeCode(email));
        user.setRecoveryCodeExpiry(new Date(System.currentTimeMillis() + 600000));
        userService.save(user);
        return getUrl("/api/v1/auth/recover/" + email + "?code=" + user.getRecoveryCode());
    }

    private String generateEnumSafeCode(String context ) {
        String dateString = new Date(System.currentTimeMillis()).toString();
        String code = context + dateString;
        return BCrypt.hashpw(code, BCrypt.gensalt());
    }
    private String getUrl(String uri){
        String address = new String ("http://localhost:8080"); //TODO: get address from config
        return address + uri;
    }


}
