package com.jungleprime.primevideo.service;

import com.jungleprime.primevideo.entity.UserPrincipal;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
@Slf4j
public class PrivilegeChecker {
    
    private PrincipalService principalService;

    public boolean HasPrivilegesOrSelfCompoundCheck(Long requestedUserId, String privilege){
        UserPrincipal userPrincipal = principalService.getCurrentUser();
        return passesSelfCheck(requestedUserId,userPrincipal) || passesReadCheck(privilege,userPrincipal) ;
    }
    public boolean passesReadCheck(String privilege, UserPrincipal currentUserPrincipal){
        return currentUserPrincipal.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(privilege));
    }
    public boolean passesSelfCheck(Long requestedUserId, UserPrincipal currentUserPrincipal) {
        return currentUserPrincipal.getId().equals(requestedUserId);
    }
}
