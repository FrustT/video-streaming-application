package com.jungleprime.primevideo.model.responses;

import com.jungleprime.primevideo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class UserResponse {

    private Long id;
    private String name;
    private String email;
    private List<String> roles;

    public UserResponse(User user){
        this.id = user.getUserId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.roles = user.getRoles().stream().map(role -> role.getName()).toList();
    }
}
