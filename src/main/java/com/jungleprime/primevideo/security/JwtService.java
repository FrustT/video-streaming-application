package com.jungleprime.primevideo.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;

@Service
@Slf4j
public class JwtService {

    private static final String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5275";
    private static final String TOKEN_ISSUER = "PrimeVideo";
    private static final int EXPIRES_IN_MINUTES = 600; // token expires in 10 hours

    public Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    public String generateToken(String subject , int expiresAfter, String tokenIssuer) {
        return Jwts.builder()
                .signWith(getSigningKey())
                .setIssuer(tokenIssuer)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * expiresAfter))
                .compact();
    }

    public String generateToken(String subject ) {
        return generateToken(subject, EXPIRES_IN_MINUTES,TOKEN_ISSUER);
    }

    /**
     * Takes token from Bearer header and returns Jws parsed claims.
     *
     * @param authHeader whole header like "Bearer eyJhbGciOiJ.."
     * @return Jws Parsed Claims
     */
    public Jws<Claims> verifyAuthHeader(String authHeader) {
        if (authHeader == null || !authHeader.startsWith("Bearer ")) return null;

        String token = authHeader.substring(7);

        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token);
    }
}
