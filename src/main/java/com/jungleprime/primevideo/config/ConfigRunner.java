package com.jungleprime.primevideo.config;

import com.jungleprime.primevideo.entity.Privilege;
import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.entity.UserRole;
import com.jungleprime.primevideo.model.requests.RegisterRequest;
import com.jungleprime.primevideo.repository.PrivilegeRepository;
import com.jungleprime.primevideo.repository.RoleRepository;
import com.jungleprime.primevideo.repository.UserRepository;
import com.jungleprime.primevideo.service.AuthenticationService;
import com.jungleprime.primevideo.service.IUserService;
import com.jungleprime.primevideo.service.PrincipalService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@AllArgsConstructor
public class ConfigRunner implements CommandLineRunner {

    //private MainInitiator initiator;
    private IUserService userService;

    private final PrivilegeRepository privilegeRepository;

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    private final PrincipalService principalService;

    private final AuthenticationService authenticationService;

    @Override
    public void run(String... args) throws Exception {

        UserRole superAdminRole = new UserRole("SUPER_ADMIN");
        roleRepository.save(superAdminRole);

        UserRole adminRole = new UserRole("ADMIN");
        roleRepository.save(adminRole);

        UserRole userRole = new UserRole("USER");
        roleRepository.save(userRole);

        UserRole guestRole = new UserRole("GUEST");
        roleRepository.save(guestRole);

        Privilege readGlobalPrivilege = new Privilege("READ_GLOBAL_PRIVILEGE", superAdminRole);
        privilegeRepository.save(readGlobalPrivilege);

        Privilege writeGlobalPrivilege = new Privilege("WRITE_GLOBAL_PRIVILEGE", superAdminRole);
        privilegeRepository.save(writeGlobalPrivilege);

        Privilege readUserPrivilege = new Privilege("READ_USER_PRIVILEGE", adminRole);
        privilegeRepository.save(readUserPrivilege);

        Privilege writeUserPrivilege = new Privilege("WRITE_USER_PRIVILEGE", adminRole);
        privilegeRepository.save(writeUserPrivilege);

        Privilege readVideoPrivilege = new Privilege("READ_VIDEO_PRIVILEGE", userRole);
        privilegeRepository.save(readVideoPrivilege);

        Privilege writeVideoPrivilege = new Privilege("WRITE_VIDEO_PRIVILEGE", adminRole);
        privilegeRepository.save(writeVideoPrivilege);

        Privilege readCategoryPrivilege = new Privilege("READ_CATEGORY_PRIVILEGE", userRole);
        privilegeRepository.save(readCategoryPrivilege);

        Privilege writeCategoryPrivilege = new Privilege("WRITE_CATEGORY_PRIVILEGE", adminRole);
        privilegeRepository.save(writeCategoryPrivilege);

        Privilege readSelfPrivilege = new Privilege("READ_SELF_PRIVILEGE",userRole);
        privilegeRepository.save(readSelfPrivilege);

        Privilege writeSelfPrivilege = new Privilege("WRITE_SELF_PRIVILEGE",userRole);
        privilegeRepository.save(writeSelfPrivilege);



        RegisterRequest registerRequestBurak = new RegisterRequest(User.builder().name("Burak").password("123").email("burak@mail.com").build());
        authenticationService.register(registerRequestBurak);

        User Burak =User.builder()
                .name("Burak")
                .password("burakpassword")
                .email("burak@mail.com")
                .verified(true)
                .roles(List.of(superAdminRole, adminRole, userRole, guestRole))
                .build();
        userService.update(userService.findByEmail("burak@mail.com").getUserId(), Burak);


        RegisterRequest registerRequestAdmin25 = new RegisterRequest(User.builder().name("admin25").password("123").email("admin25@mail.com").build());
        authenticationService.register(registerRequestAdmin25);

        User admin25 = User.builder()
                .name("admin25")
                .password("admin25password")
                .email("admin25@mail.com")
                .verified(true)
                .roles(List.of(adminRole, userRole, guestRole))
                .build();
        userService.update(userService.findByEmail("admin25@mail.com").getUserId(), admin25);



        RegisterRequest registerRequestUser1 = new RegisterRequest(User.builder().name("user1").password("123").email("user1@mail.com").build());
        authenticationService.register(registerRequestUser1);

        User user1 = User.builder()
                .name("user1")
                .password("user1password")
                .email("user1@mail.com")
                .verified(true)
                .roles(List.of(userRole, guestRole))
                .build();

        userService.update(userService.findByEmail("user1@mail.com").getUserId(), user1);
    }

}
