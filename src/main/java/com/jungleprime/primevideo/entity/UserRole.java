package com.jungleprime.primevideo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;
import org.hibernate.annotations.NaturalId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Table(name = "roles")
@Entity
@AllArgsConstructor
@RequiredArgsConstructor
public class UserRole implements Serializable {

    @Id
    @SequenceGenerator(name = "role_generator", sequenceName = "role_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
    private Long roleId;
    @NaturalId
    private String name;

    @ManyToMany(mappedBy = "roles", fetch=FetchType.EAGER)
    @Column(name = "fk_user_id", nullable = true)
    //@JoinColumn(name = "fk_user_id")
    private List<User> users = new ArrayList<>();

    @OneToMany(mappedBy = "role", fetch=FetchType.EAGER)
    @Column(name = "fk_privilege_id", nullable = true)
    //@JoinColumn(name = "fk_privilege_id")
    private List<Privilege> privileges = new ArrayList<>();

    public UserRole(String name) {
        this.name = name;
    }

}
