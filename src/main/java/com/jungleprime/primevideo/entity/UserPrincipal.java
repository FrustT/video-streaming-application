package com.jungleprime.primevideo.entity;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

@AllArgsConstructor
public class UserPrincipal implements UserDetails {


    private Long id;
    private String username;
    private String password;
    private Boolean verified;
    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(User user) {
        this.id = user.getUserId();
        this.username = user.getEmail();
        this.password = user.getPassword();
        this.verified = user.isVerified();
        this.authorities = extractAuthorities(user);
    }

    public static UserPrincipal fromUser(User user) {
        return new UserPrincipal(user);
    }
    private Collection<? extends GrantedAuthority> extractAuthorities(User user) {
        Collection<GrantedAuthority> usersGrantedAuthorities = new HashSet<>();

        for (UserRole role : user.getRoles()) {
            for (Privilege privilege : role.getPrivileges()) {
                usersGrantedAuthorities.add(new SimpleGrantedAuthority(privilege.getName()));
            }
        }

        return usersGrantedAuthorities;
    }
    public String toString(){
        return "UserPrincipal [id=" + id + ", username=" + username + ", password=" + password + ", verified=" + verified + ", authorities=" + authorities + "]";
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isEnabled() {
        return this.verified;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
