package com.jungleprime.primevideo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "users")
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class User implements Serializable {

    @Id
    @SequenceGenerator(name = "user_generator", sequenceName = "user_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    private Long userId;
    private String name;
    private String email;
    private String password;

    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<UserRole> roles;

    private Boolean verified;
    private String verificationCode;
    private Date verificationCodeExpiry;

    private String recoveryCode;
    private Date recoveryCodeExpiry;



    // @OneToMany(mappedBy = "user")
    // private List<Invoice> invoices;

    // @ManyToMany
    // private List<Categories> starredCategories;

    // @ManyToMany
    // private List<Content> starredContent;

    public boolean isVerified() {
        return verified;
    }

    @Override
    public String toString() {
        return "User: " + name + " email: " + email + " pass: " + password + " roles: " + roles + " verified:" + verified + "verCode: " + verificationCode + "verCodeExp: " + verificationCodeExpiry + " " + recoveryCode + " " + recoveryCodeExpiry;
    }

}
