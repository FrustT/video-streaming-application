package com.jungleprime.primevideo.entity;

import java.io.Serializable;

import org.hibernate.annotations.NaturalId;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "privileges")
@AllArgsConstructor
@RequiredArgsConstructor
public class Privilege implements Serializable {

    @Id
    @SequenceGenerator(name = "privilege_generator", sequenceName = "privilege_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "privilege_generator")
    private Long id;

    @Column(nullable = false, unique = true)
    @NaturalId
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_role_id", nullable = false)
    private UserRole role;

    public Privilege(String name) {
        this.name = name;
    }

    public Privilege(String name, UserRole role) {
        this.name = name;
        this.role = role;
    }

}
