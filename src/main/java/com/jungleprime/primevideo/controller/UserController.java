package com.jungleprime.primevideo.controller;

import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.model.responses.UserResponse;
import com.jungleprime.primevideo.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final IUserService userService;

    @GetMapping//admin only
    @PreAuthorize("hasAuthority('READ_USER_PRIVILEGE')")
    public ResponseEntity<Collection<UserResponse>> getAllUsers() {
        return ResponseEntity.ok(userService.findAll());
    }



    @GetMapping("/{id}")
    @PreAuthorize("@privilegeChecker.HasPrivilegesOrSelfCompoundCheck(#id, 'READ_USER_PRIVILEGE')")
    public ResponseEntity<UserResponse> getUserData(@PathVariable("id") @Param("id") Long userId) {
        return ResponseEntity.ok(new UserResponse(userService.findById(userId)));
    }
    @PutMapping("/{id}")
    @PreAuthorize("@privilegeChecker.HasPrivilegesOrSelfCompoundCheck(#id, 'WRITE_USER_PRIVILEGE')")
    public ResponseEntity<UserResponse> updateUserData(@PathVariable("id") Long id, @RequestBody User user) {
        return ResponseEntity.ok(new UserResponse(userService.update(id, user)));
    }

    /*
    @GetMapping("/{id}/invoice")//admin only if not self
    public ResponseEntity<Collection<InvoiceResponse>> getUserInvoices(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.findById(id).getInvoices().stream().map(InvoiceResponse::new).toList());
    }
    @PostMapping("/{id}/subscribe/{id}")//only self
    */

}
