package com.jungleprime.primevideo.controller;

import com.jungleprime.primevideo.model.requests.AuthenticationRequest;
import com.jungleprime.primevideo.model.requests.RegisterRequest;
import com.jungleprime.primevideo.model.responses.AuthenticationResponse;
import com.jungleprime.primevideo.service.AuthenticationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> login(@Valid @RequestBody RegisterRequest request) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    @GetMapping("/verify/{username}")
    public ResponseEntity<String> verify(@RequestParam("code") String verificationCode, @PathVariable("username") String username) {
        authenticationService.verify(username,verificationCode);
        return ResponseEntity.ok("User Verified");
    }

    @PostMapping("/verify/{username}")
    public ResponseEntity<String> verifyByEmail( @PathVariable("username") String username) {
        String link = authenticationService.verifyRequest(username);
        return ResponseEntity.ok("Verification Link has been sent to your email " + link);
    }
    @PutMapping("/recover/{username}")
    public ResponseEntity<String> recover( @PathVariable("username") String username, @RequestParam("code") String recoveryCode, @RequestBody String password) {
        authenticationService.recoverPassword(username, recoveryCode, password);
        return ResponseEntity.ok("Password Changed");
    }
    @PostMapping("/recover/{username}")
    public ResponseEntity<String> recoverRequest( @PathVariable("username") String username) {
        String link = authenticationService.recoverPasswordRequest(username);
        return ResponseEntity.ok("Recovery Link has been sent to your email " + link);
    }

}