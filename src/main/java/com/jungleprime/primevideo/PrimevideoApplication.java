package com.jungleprime.primevideo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimevideoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimevideoApplication.class, args);
	}

}
