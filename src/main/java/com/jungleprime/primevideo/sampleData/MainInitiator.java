package com.jungleprime.primevideo.sampleData;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.jungleprime.primevideo.entity.User;
import com.jungleprime.primevideo.entity.UserRole;
import com.jungleprime.primevideo.entity.Privilege;

import com.jungleprime.primevideo.repository.PrivilegeRepository;
import com.jungleprime.primevideo.repository.RoleRepository;
import com.jungleprime.primevideo.repository.UserRepository;

import lombok.AllArgsConstructor;

public class MainInitiator {
    private  PrivilegeRepository privilegeRepository;
    private  RoleRepository roleRepository;
    private  UserRepository userRepository;

    public void init() {
        initPrivileges();
        initUsers();
        initRoles();
    }

    private void initPrivileges() {

        Privilege readGlobalPrivilege = new Privilege("READ_GLOBAL_PRIVILEGE");
        privilegeRepository.save(readGlobalPrivilege);

        Privilege writeGlobalPrivilege = new Privilege("WRITE_GLOBAL_PRIVILEGE");
        privilegeRepository.save(writeGlobalPrivilege);

        Privilege readUserPrivilege = new Privilege("READ_USER_PRIVILEGE");
        privilegeRepository.save(readUserPrivilege);

        Privilege writeUserPrivilege = new Privilege("WRITE_USER_PRIVILEGE");
        privilegeRepository.save(writeUserPrivilege);

        Privilege readVideoPrivilege = new Privilege("READ_VIDEO_PRIVILEGE");
        privilegeRepository.save(readVideoPrivilege);

        Privilege writeVideoPrivilege = new Privilege("WRITE_VIDEO_PRIVILEGE");
        privilegeRepository.save(writeVideoPrivilege);

        Privilege readCategoryPrivilege = new Privilege("READ_CATEGORY_PRIVILEGE");
        privilegeRepository.save(readCategoryPrivilege);

        Privilege writeCategoryPrivilege = new Privilege("WRITE_CATEGORY_PRIVILEGE");
        privilegeRepository.save(writeCategoryPrivilege);

        Privilege readSelfPrivilege = new Privilege("READ_SELF_PRIVILEGE");
        privilegeRepository.save(readSelfPrivilege);

        Privilege writeSelfPrivilege = new Privilege("WRITE_SELF_PRIVILEGE");
        privilegeRepository.save(writeSelfPrivilege);

    }

    private void initRoles() {

    }

    private void initUsers() {


    }


}
